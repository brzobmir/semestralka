#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <thread>
#include <atomic>
#include <vector>
#include <random>
#include <chrono>
#include <algorithm>
#include <string>
#include <cstring>
#include <fstream>


namespace timer {
    auto now() {
        return std::chrono::high_resolution_clock::now();
    }

    template <typename TimePoint>
    long long to_ms(TimePoint tp) {
        return std::chrono::duration_cast<std::chrono::milliseconds>(tp).count();
    }
}

double get_random_double() {
    static std::mt19937 mt{ std::random_device{}() };
    static std::uniform_real_distribution<> dist(-1000, 1000);
    return dist(mt);
}

int get_random_int(){
    std::mt19937 rng{std::random_device{}()};
    std::uniform_int_distribution<std::mt19937::result_type> dist6(1, 9);
    return static_cast<int>(dist6(rng));
}

void editRow(std::vector<double> &base, std::vector<double> &to_change, unsigned long position){
    double key = to_change.at(position - 1) / base.at(position - 1);
    bool first = true;
    for (unsigned long i = position - 1; i < base.size(); ++i) {
        if (first){
            to_change.at(i) = 0;
            first = false;
        } else {
            to_change.at(i) -= (key * base.at(i));
        }
    }
}

void mt_editRow(std::vector<std::vector<double>>& matrix, int from, int to, unsigned long position){
    for (int i = from; i < to + 1; ++i) {
        editRow(matrix.at(position - 1), matrix.at(static_cast<unsigned long>(i)), position);
    }
}

long double st_elimination(std::vector<std::vector<double>>& matrix) {
    unsigned long position = 1;
    long double result = 1;
    while (position != matrix.size()){
        for (unsigned long i = 0; i < matrix.size() - position; ++i) {
            editRow(matrix.at(position - 1), matrix.at(position + i), position);
        }
        result *= matrix.at(position - 1).at(position - 1);
        position++;
    }
    result *= matrix.at(matrix.size() - 1).at(matrix.size() - 1);

    std::ofstream outfile;
    outfile.open("st_elimination.txt", std::ios::out | std::ios::trunc );
    for(unsigned long i = 0; i < matrix.size(); ++i) {
        for (unsigned long j = 0; j < matrix.size(); ++j) {
            outfile << matrix.at(i).at(j) << " ";
        }
        outfile << std::endl;
    }
    outfile.close();
    return result;
}

long double mt_elimination(std::vector<std::vector<double>>& matrix) {
    if (matrix.size() <= 100){
        return st_elimination(matrix);
    }
    unsigned long position = 1;
    long double result = 1;
    while (position != matrix.size()){
        unsigned long count = matrix.size() - position;
        if (count % 2 == 0){
                std::thread t1(mt_editRow, std::ref(matrix), position, (count / 2) + (position - 1), position);
                std::thread t2(mt_editRow, std::ref(matrix), (count / 2) + 1 +(position - 1), count + (position - 1), position);
                t1.join();
                t2.join();
        } else {
                std::thread t1(mt_editRow, std::ref(matrix), position, (count / 2) + (position - 1), position);
                std::thread t2(mt_editRow, std::ref(matrix), (count / 2) + 1 + (position - 1), count + (position - 1) - 1, position);
                t1.join();
                t2.join();
                editRow(matrix.at(position - 1), matrix.at(count + (position - 1)), position);
        }

        result *= matrix.at(position - 1).at(position - 1);
        position++;
    }
    result *= matrix.at(matrix.size() - 1).at(matrix.size() - 1);

    std::ofstream outfile;
    outfile.open("mt_elimination.txt", std::ios::out | std::ios::trunc );
    for(unsigned long i = 0; i < matrix.size(); ++i) {
        for (unsigned long j = 0; j < matrix.size(); ++j) {
            outfile << matrix.at(i).at(j) << " ";
        }
        outfile << std::endl;
    }
    outfile.close();
    return result;
}

void print_usage() {
    std::clog << "Nacházíte se v aplikaci pro výpočet determinantu čtvercové matice. Aplikace přijímá na vstupu jeden \n"
                 "parametr a to velikost matice, aplikace nepřijímá matici, jako takou, pouze jí generuje a následně \n"
                 "vygenerovanou matici vyřeší. Zároveň vygenerovanou matici a matici upravenou do horního blokového tvaru \n"
                 "vypisuje do souboru. \n"
                 "Pokud parametr velikosti nebude zadán, tak se vygeneruje a vyřeší defaultně \n"
                 "nastavená matice o velikosti 500x500. \n"
                 "Aplikace vyřeší matici jak jedním vláknem, tak i dvěma vlákny, výsledek a porovnání se následně vypíše. \n"
                 "Rychlost vícevláknové implementace se projeví až při vyšších velikostech matic, zhruba od matic velikosti \n"
                 "600x600 začne být rychlejší a rychlejší vícevláknová implementace. \n";
}

bool is_help(std::string const& argument) {
    return argument == "--help" || argument == "-h";
}

int main(int argc, char** argv) {
    int matrix_size = 0;
    
    if (std::any_of(argv, argv+argc, is_help)) {
        print_usage();
        return 0;
    }
    
    if (argc == 1) {
        std::clog << "Parametr velikosti nezadán, bude vygenerována a vyřešena matice 1000x1000. Chvilku strpení. \n";
        matrix_size = 1000;
    } else if (argc >= 3) {
        std::clog << "Příliš mnoho argumentů.\n";
        print_usage();
        return 1;
    } else {
        std::size_t endpos = 0;
        matrix_size = std::stoi(argv[1], &endpos);
        if (endpos != std::strlen(argv[1])) {
            std::clog << "Parametr velikosti, '" << argv[1] <<  "', není validní číslo.\n";
            return 2;
        }
        std::clog << "Matrix " << matrix_size << "x" << matrix_size << " bude vygenerována a vyřešena. "
                                                                       "Chvilku strpení. \n";
    }

    std::vector<std::vector<double>> st_random_matrix;
    std::vector<std::vector<double>> mt_random_matrix;
    for (int i = 0; i < matrix_size; ++i) {
        std::vector<double> line;
        line.reserve(static_cast<unsigned long>(matrix_size));
        for (int j = 0; j < matrix_size; ++j) {
            line.push_back(get_random_int());
        }
        st_random_matrix.push_back(line);
        mt_random_matrix.push_back(line);
    }
    std::ofstream outfile;
    outfile.open("file.txt", std::ios::out | std::ios::trunc );
    for(int i = 0; i < matrix_size; ++i) {
        for (int j = 0; j < matrix_size; ++j) {
            outfile << st_random_matrix.at(i).at(j) << " ";
        }
        outfile << std::endl;
    }
    outfile.close();

    auto t1 = timer::now();
    auto st_inside = st_elimination(st_random_matrix);
    auto t2 = timer::now();
    auto mt_inside = mt_elimination(mt_random_matrix);
    auto t3 = timer::now();

    std::cout << "ST determinant: " << st_inside << '\n';
    std::cout << "MT determinant: " << mt_inside << '\n';
    std::cout << "ST time needed: " << timer::to_ms(t2-t1) << "ms" << '\n';
    std::cout << "MT time needed: " << timer::to_ms(t3-t2) << "ms" << '\n';
}