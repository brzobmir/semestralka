# Determinant čtvercové matice

Determinant čtvercové matice je program pro výpočet determinantu čtvercové matice 
pomocí gaussovy eliminaci.

## Implementace

Implementace pro jedno vlákno je v podstatě triviální, kdy jedno vlákno postupně
upraví všechny řádky tak, aby byla matice v horním blokovém tvaru. Vícevláknová implementace
pak vydělí počet řádků v jedné iteraci dvěma a přidělí půlku matice jednomu vláknu a druhou půlku
druhému vláknu, jelikož uprávy jsou v každé iteraci na sobě nezávislé, tak nemůže nastat
problém se synchronizací.

## Měření

Z měření mém počítači mi vyšlo, že jednovláknová varianta potřebuje pro výpočet determinantu matice 1000x1000 13529ms a vícevláknová varianta se 2 vlákny pro stejnou matici potřebuje 11508ms.
Časy se samozřejmě při každém výpočtu lehce mění.

Výstup z měření
```
ST determinant: -inf
MT determinant: -inf
ST time needed: 13529ms
MT time needed: 11508ms
```

Z měření na gitlabu vyšlo, že jednovláknová varianta potřebuje pro výpočet 1051ms a vícevláknová varianta 958ms.

Výstup z měření
```
ST determinant: -2.74519e+1695
MT determinant: -2.74519e+1695
ST time needed: 1051ms
MT time needed: 958ms
```